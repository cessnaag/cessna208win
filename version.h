#ifndef VERSION_H
#define VERSION_H

#define VER_FILEVERSION             2,1,7,0
#define VER_FILEVERSION_STR         "2.1.7.0"

#define VER_PRODUCTVERSION          2,1,7,0
#define VER_PRODUCTVERSION_STR      "2.1.7.0"

#define VER_COMPANYNAME_STR         "pilot40"
#define VER_FILEDESCRIPTION_STR     "cessna208win"
#define VER_INTERNALNAME_STR        "cessna208win"
#define VER_LEGALCOPYRIGHT_STR      "Copyright � 2017 pilot40"
#define VER_LEGALTRADEMARKS1_STR    "All Rights Reserved"
#define VER_LEGALTRADEMARKS2_STR    VER_LEGALTRADEMARKS1_STR
#define VER_ORIGINALFILENAME_STR    "cessna208win.exe"
#define VER_PRODUCTNAME_STR         "cessna208win"

#define VER_COMPANYDOMAIN_STR       "pilot208.ru"

#endif // VERSION_H